import pandas as pd


def get_highest_degree(row):
    degrees = [row['degree_1'], row['degree_2'], row['degree_3']]
    if 'PhD' in degrees:
        return 'PhD' 
    elif 'Masters' in degrees:
        return 'Masters' 
    elif 'Bachelors' in degrees:
        return 'Bachelors' 
    return 'None'


if __name__ == "__main__":
    
    # Prepare the data
    recruiting_activity = pd.read_excel('Case Study Data - Comp Research Associate.xlsx', sheet_name='recruiting_activity')
    candidate_profile = pd.read_excel('Case Study Data - Comp Research Associate.xlsx', sheet_name='candidate_profile')
    df = recruiting_activity.merge(candidate_profile, left_on='cand_id', right_on='cand_id', how='left')
    df['highest_degree'] = df.apply(get_highest_degree, axis=1)
    df['yrs_exp_group'] = df['yrs_exp'].round() // 5 * 5
    df['furthest_stage'] = pd.Categorical(df['furthest_stage'], categories=['Offer Sent', 'In-House Interview', 'Phone Screen', 'New Application'], ordered=True)
    df['highest_degree'] = pd.Categorical(df['highest_degree'], categories=['PhD', 'Masters', 'Bachelors'], ordered=True)
    df = df.sort_values(by=['furthest_stage']).drop_duplicates(subset='cand_id', keep='last')

    # Display candidates moving through recruiting stages
    print(f"""\n\nSee candidates moving through recruiting stages by highest degree
\n{df.groupby(['furthest_stage', 'highest_degree']).agg({'cand_id': 'nunique'}).unstack().sort_index(ascending=False).fillna(0)}
\n{df.groupby(['furthest_stage', 'highest_degree']).agg({'cand_id': 'nunique'}).apply(lambda x: x/x.sum()).unstack().sort_index(ascending=False).fillna(0)}""")
    
    # Investigate salaries
    print(f"""\n\nApplicants who have a PhD are not expected to make more \n{df.groupby(['highest_degree']).agg({'comp_exp': 'mean'}).sort_index(ascending=False).fillna(0).head()}""")
    print(f"""\n\nCompensation expectations differ accross departments \n{df.groupby(['highest_degree', 'dept']).agg({'comp_exp': 'mean'}).sort_values(by='comp_exp', ascending=False).unstack()}""")
    print(f"""\n\nApplicants who are expected to be paid more have 20+ years of experience, higher degrees, and work in technology and finance \n{df.groupby(['highest_degree', 'yrs_exp_group', 'dept', 'furthest_stage']).agg({'comp_exp': 'mean'}).sort_values(by=['comp_exp', 'yrs_exp_group', 'highest_degree', 'furthest_stage'], ascending=False).dropna().head(10)}""")
    print(f"""\n\nApplicants who are expected to earn less have little experience, and work in technology, finance and product \n{df.groupby(['highest_degree', 'yrs_exp_group', 'dept', 'furthest_stage']).agg({'comp_exp': 'mean'}).sort_values(by=['comp_exp', 'yrs_exp_group', 'highest_degree', 'furthest_stage'], ascending=False).dropna().tail(10)}""")
    print(f"""
- To analyze the recruiting activity, we deduplicated the data by candidate ID and kept the most recent stage for each candidate. 
- We then grouped the data by furthest stage, highest degree, and years of experience to investigate the salaries associated with each group. 
- We found that applicants who are expected to be paid more have 20+ years of experience, higher degrees, and work in technology and finance. 
- We also found that applicants who are expected to earn less have little experience, and work in technology, finance and product.
""")

    # Communicate with the team manager
    print(f"""
    Dear Analytics Team Manager,

I am writing to you in response to the COO's request to investigate whether the compensation we offer for engineering and sales roles is appropriate.

To answer this question, I propose to analyze internal and external data to determine if the compensation we offer is in line with the market rate. Specifically, I plan to analyze the following data:

Internal Data:
- Historical data on compensation for engineering and sales roles
- Information on the qualifications and experience of the candidates we have hired for these roles

External Data:
- Market rate data from salary surveys and job postings
- Data from industry reports and studies

From this analysis, I will be able to determine whether the compensation we offer is in line with the market rate. Additionally, I will be able to identify any discrepancies between the qualifications and experience of the candidates we hire and the compensation we offer.

The most important points for the COO to know are:
- Whether the compensation we offer is in line with the market rate
- Any discrepancies between the qualifications and experience of the candidates we hire and the compensation we offer

I believe this analysis will provide the COO with the information they need to determine whether the compensation we offer for engineering and sales roles is appropriate.

Please let me know if you have any questions or would like any additional information.

Sincerely,
Tristán Gramsch
""")
