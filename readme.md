# D.E. Shawn case study
The .docx file provides instructions for a case study for a Compensation Research Analyst. The case study requires the use of R or Python to join two datasets, create a Recruiting Funnel view, and answer questions about compensation expectations. Additionally, the candidate is asked to draft an email to their analytics team manager outlining an analysis plan to answer a question from the COO about whether engineering and sales roles are priced properly.


# Find in case_study.md
Investigate hired people. 
Figure out the best candidates.
Communicate to the head of recruiting and COO.
Replicate the results by running run.py. Requieres pandas. 
