# Compensation Research.


# 2


See candidates moving through recruiting stages by highest degree

                   cand_id                  
highest_degree         PhD Masters Bachelors
furthest_stage                              
New Application        354     844      2185
Phone Screen           264     170       338
In-House Interview     261     171       248
Offer Sent              30      30        64

                     cand_id                    
highest_degree           PhD   Masters Bachelors
furthest_stage                                  
New Application     0.071385  0.170196  0.440613
Phone Screen        0.053237  0.034281  0.068159
In-House Interview  0.052632  0.034483  0.050010
Offer Sent          0.006050  0.006050  0.012906


# 3


Applicants who have a PhD are not expected to make more 
                    comp_exp
highest_degree              
Bachelors       70606.525573
Masters         74604.032922
PhD             72447.084708


Compensation expectations differ accross departments 
                    comp_exp                                                        
dept             Engineering       Finance            IT       Product         Sales
highest_degree                                                                      
PhD             72212.866450  77026.086957  64905.882353  70966.666667  78435.294118
Masters         64071.428571  78391.812865  74140.425532  70800.980392  79540.572391
Bachelors       63545.099668  75756.877323  75809.333333  69544.104803  79307.237636


Applicants who are expected to be paid more have 20+ years of experience, higher degrees, and work in technology and finance 
                                                                  comp_exp
highest_degree yrs_exp_group dept        furthest_stage                   
PhD            20.0          Engineering In-House Interview  130466.666667
Masters        25.0          IT          Phone Screen        100600.000000
                             Finance     In-House Interview   99700.000000
                                         New Application      99000.000000
Bachelors      25.0          Finance     New Application      97300.000000
                                         In-House Interview   96850.000000
Masters        25.0          IT          New Application      96650.000000
PhD            25.0          IT          New Application      96600.000000
               20.0          Product     New Application      96400.000000
Bachelors      25.0          IT          New Application      95800.000000


Applicants who are expected to earn less have little experience, and work in technology, finance and product 
                                                             comp_exp
highest_degree yrs_exp_group dept        furthest_stage              
PhD            0.0           Finance     Phone Screen         53600.0
Bachelors      0.0           Finance     Phone Screen         53220.0
PhD            0.0           IT          New Application      52200.0
                                         In-House Interview   52100.0
                             Finance     New Application      51400.0
Masters        0.0           Finance     In-House Interview   51300.0
Bachelors      0.0           Finance     In-House Interview   51250.0
Masters        0.0           IT          New Application      51200.0
Bachelors      0.0           IT          New Application      49500.0
                             Engineering New Application       1500.0

- To analyze the recruiting activity, we deduplicated the data by candidate ID and kept the most recent stage for each candidate. 
- We then grouped the data by furthest stage, highest degree, and years of experience to investigate the salaries associated with each group. 
- We found that applicants who are expected to be paid more have 20+ years of experience, higher degrees, and work in technology and finance. 
- We also found that applicants who are expected to earn less have little experience, and work in technology, finance and product.


# 4


    Dear Analytics Team Manager,

I am writing to you in response to the COO's request to investigate whether the compensation we offer for engineering and sales roles is appropriate.

To answer this question, I propose to analyze internal and external data to determine if the compensation we offer is in line with the market rate. Specifically, I plan to analyze the following data:

Internal Data:
- Historical data on compensation for engineering and sales roles
- Information on the qualifications and experience of the candidates we have hired for these roles

External Data:
- Market rate data from salary surveys and job postings
- Data from industry reports and studies

From this analysis, I will be able to determine whether the compensation we offer is in line with the market rate. Additionally, I will be able to identify any discrepancies between the qualifications and experience of the candidates we hire and the compensation we offer.

The most important points for the COO to know are:
- Whether the compensation we offer is in line with the market rate
- Any discrepancies between the qualifications and experience of the candidates we hire and the compensation we offer

I believe this analysis will provide the COO with the information they need to determine whether the compensation we offer for engineering and sales roles is appropriate.

Please let me know if you have any questions or would like any additional information.

Sincerely,
Tristán Gramsch


